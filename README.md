# Simple React Native Starter Template 

## Description

This is a highly opinionated boilerplace that serves as a starter template for my own react native projects.


## Screens

This application includes the following screens 

### Auth Screens

- Login
- Signup
- Forgot Password


### Root Screens

- Home 
- Settings



## Screenshots
Will be updated later

## Libraries to check
- [React Native Snap Carousel](https://github.com/archriss/react-native-snap-carousel)
- [App Intro](https://github.com/FuYaoDe/react-native-app-intro)
- [React Native Paper (Material UI)](https://callstack.github.io/react-native-paper/index.html)
- [Victory Chart](https://formidable.com/open-source/victory/gallery)
- [React Native Notifier](https://github.com/seniv/react-native-notifier)
- [React Native Bottom](https://github.com/nysamnang/react-native-raw-bottom-sheet)
- [React Native Webview](https://github.com/react-native-community/react-native-webview)