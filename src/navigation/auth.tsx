import React from 'react'
import LoginScreen from '../screens/auth/login'
import { createStackNavigator } from '@react-navigation/stack';
import SignupScreen from '../screens/auth/signup';
import ForgotPassword from '../screens/auth/forgot_password';


const Stack = createStackNavigator();


const AuthNavigator = () => (

  <Stack.Navigator headerMode='none'>
    <Stack.Screen name='Login' component={LoginScreen} />
    <Stack.Screen name='Signup' component={SignupScreen} />
    <Stack.Screen name='ForgotPassword' component={ForgotPassword} />
  </Stack.Navigator>
)


export default AuthNavigator
