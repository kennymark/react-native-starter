import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import SearchScreen from '../screens/search'
import SettingsScreen from '../screens/settings'

import AsyncStorage from '@react-native-community/async-storage';
import { StoreProvider } from 'easy-peasy'
import tw from 'tailwind-rn'
import Icon from 'react-native-ionicons'
import store from '../shared/store';

import AuthStackNavigator from './auth';
import HomeStack from './home';

const Tab = createBottomTabNavigator()

function getIcon(iconName, focused) {
  return (
    <Icon name={iconName} size={20} color={focused ? 'black' : 'darkgray'} />
  )
}




const RootNavigator = () => (

  <Tab.Navigator
    tabBarOptions={{
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
      style: tw('bg-gray-100 h-24'),
      labelStyle: tw('font-semibold text-xs'),
    }}>
    <Tab.Screen name="Home" component={HomeStack} options={{
      tabBarIcon: ({ focused }) => getIcon('home-outline', focused)
    }} />
    <Tab.Screen name="Search" component={SearchScreen} options={{
      tabBarIcon: ({ focused }) => getIcon('search', focused)
    }} />

    <Tab.Screen name="Settings" component={SettingsScreen} options={{
      tabBarIcon: ({ focused }) => getIcon('settings-outline', focused)
    }} />
  </Tab.Navigator>
)


function AppContainer() {
  const [hasToken, setHasToken] = useState(null)

  useEffect(() => {
    AsyncStorage.setItem('token', 'heyy')
    // AsyncStorage.removeItem('token')
    AsyncStorage.getItem('token')
      .then((token: any) => {
        if (token) setHasToken(token)
      }).catch(err => setHasToken(null))

    console.log({ user: hasToken, store: AsyncStorage.getAllKeys() })
  }, [])

  return (
    <NavigationContainer>

      <StoreProvider store={store}>
        {hasToken ?
          <RootNavigator /> :
          <AuthStackNavigator />
        }
      </StoreProvider>

    </NavigationContainer>
  );
}



export default AppContainer