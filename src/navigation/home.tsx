import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/home/home';
import TestPageScreen from '../screens/home/test-page';


const Stack = createStackNavigator();


const HomeStack = () => (

  <Stack.Navigator headerMode='none' >
    <Stack.Screen name='Home' component={HomeScreen} />
    <Stack.Screen name='Test' component={TestPageScreen} />
  </Stack.Navigator>

)


export default HomeStack
