


import React from 'react'
import { View, Text } from 'react-native'
import tw from 'tailwind-rn';
import { SafeAreaView } from 'react-native-safe-area-context';

const Layout = ({ title, children }) => {
  return (
    <SafeAreaView>
      <Text style={tw('text-3xl p-4 font-semibold')}>{title}</Text>
      <View style={tw('p-4')}>
        {children}
      </View>
    </SafeAreaView>
  )
}

export default Layout
