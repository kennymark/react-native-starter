import React from 'react'
import { Text } from 'react-native'
import tw from 'tailwind-rn'


interface Error {
  field: string,
  message: string,
  validation: string,
}

interface Props {
  errors: Error[] | null,
  field: string;
}


const FormErrorMessage = ({ errors, field }: Props) => {

  function get(errors, field) {
    let error
    if (errors) {
      error = errors.find(err => err.field === field)
    }
    return (
      <>{error.message}</>
    )
  }

  function hasError(errors, field) {
    if (errors && errors.find(err => err.field === field)) {
      return true
    }
    return false
  }

  return <>
    {hasError(errors, field) &&
      <Text style={tw('py-1 text-red-400 self-start normal-case')}>
        {get(errors, field)}
      </Text>
    }
  </>
}


export default FormErrorMessage
