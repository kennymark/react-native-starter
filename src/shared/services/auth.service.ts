import http from 'redaxios'
import AsyncStorage from '@react-native-community/async-storage';

class AuthService {
  protected url = 'http://localhost:3333/api/account'

  protected getToken(): string {
    return AsyncStorage.getItem('token') as any
  }

  public isAuthenticated(): boolean {
    return !!AsyncStorage.getItem('token');
  }

  login(data): Promise<any> {
    return http.post(this.url + '/login', data)
  }

  register(data): Promise<any> {
    return http.post(this.url + '/register', data)
  }

  forgotPassword(data): Promise<any> {
    return http.post(this.url + '/forgot-password', data)
  }

  resetPassword(data): Promise<any> {
    return http.post(this.url + '/reset-password', data)
  }

  resetPassword2(data): Promise<any> {
    return http.post(this.url + '/reset-password/markcoffiekenneth@gmail.com?signature=eyJtZXNzYWdlIjoiL2FwaS9hY2NvdW50L3Jlc2V0LXBhc3N3b3JkL21hcmtjb2ZmaWVrZW5uZXRoQGdtYWlsLmNvbSIsImV4cGlyeURhdGUiOiIyMDIwLTA1LTEzVDIxOjM4OjU3Ljk5MloifQ.FbeOm0Qbu_UdHj9OrUUJN0HM2MkePWekvgppSoTQJyE', data)
  }

  logout(data): Promise<any> {
    return http.post(this.url + '/logout', data)
  }

}

export default new AuthService();