import { createStore } from 'easy-peasy'
import authModel from './auth'

const store = createStore({
  auth: authModel,
  user: '',
})


export default store
