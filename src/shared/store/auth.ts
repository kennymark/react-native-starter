import { createStore, action, Action, } from "easy-peasy";



const authModel: Partial<AuthState> = {

  isSignedIn: false,
  user: null,
  token: null,

  setUser: action((state, payload) => {
    state.user = payload
  }),

  setSignedIn: action((state, payload) => {
    state.isSignedIn = payload
  }),

  setToken: action((state, payload) => {
    state.token = payload
  })
};



export interface AuthState {
  isSignedIn: boolean;
  user: User | null;
  token: string | null;
  setUser: Action<{}, User>;
  setSignedIn: Action<{}, any>
  setToken: Action<{}, any>

}


export interface User {
  name: string;
  email: string;
  password: string;
  createdAt: string;
  updatedAt: string;
}

export default authModel