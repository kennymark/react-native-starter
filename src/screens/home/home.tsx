import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Layout from '../../components/Layout'
import AsyncStorage from '@react-native-community/async-storage';
import tw from 'tailwind-rn'


export default function HomeScreen({ navigation: { navigate } }) {
  const [token, setToken] = useState('')

  const testData = ['ken', 'marthe', 'jason']

  useEffect(() => {
    setToken(JSON.stringify(AsyncStorage.getItem('token')))
  }, [])

  return (
    <Layout title="Home">


      <TouchableOpacity style={tw('bg-red-600 rounded mt-10')} onPress={() => navigate('Test', { testData })}>
        <Text style={tw('py-4 text-gray-100 text-center')}> Go to Page 2</Text>
      </TouchableOpacity>

    </Layout>
  );
}
