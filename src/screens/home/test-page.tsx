import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Layout from '../../components/Layout'
import tw from 'tailwind-rn'
import { upperFirst } from 'lodash'


const TestPage = ({ route: { params }, navigation }) => {

  const [data, setData] = useState([])

  useEffect(() => {
    setData(params.testData)

  }, [])


  return (
    <Layout title='Test Page'>

      {data && data.map(item => (
        <Text key={item} style={tw('mb-3')}>{upperFirst(item)}</Text>
      ))}

      <TouchableOpacity style={tw('bg-yellow-600 rounded mt-10')} onPress={() => navigation.navigate('Search')}>
        <Text style={tw('py-4 text-gray-100 text-center')}> Go to Search</Text>
      </TouchableOpacity>

    </Layout>
  )
}

export default TestPage
