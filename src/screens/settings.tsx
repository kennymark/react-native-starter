import React, { useEffect } from 'react'
import { Text, View, ScrollView } from 'react-native'
import Layout from '../components/Layout'
import tw from 'tailwind-rn'
import { upperFirst, upperCase } from 'lodash'
const SettingsScreen = (props) => {

  const options = [
    {
      title: 'general', screens: [
        { name: 'Notifications', description: 'Manage your notifications' },
      ]
    },

    {
      title: 'user control', screens: [
        { name: 'Appearance', description: 'Change the application appearance' },
      ]
    },
    {
      title: 'about', screens: [
        { name: 'help', description: 'Get some help' },
        { name: 'change log', description: 'View Changelog' },
        { name: 'version', description: '3.456' },
      ]
    }


  ]

  useEffect(() => {
  }, [])

  return (
    <Layout title="Settings">
      <ScrollView>

        {options.map(item => (
          <View key={item.title}>
            <Text style={tw('font-bold text-base mb-3 mt-5')}>
              {upperFirst(item.title)}
            </Text>
            <SettingItem item={item} />
          </View>

        ))}

      </ScrollView>
    </Layout>
  )
}

function SettingItem({ item }) {
  return (
    <View >
      {item.screens.map(item => (

        <View key={item.name} style={tw('border-b border-gray-300 py-1 rounded-md mb-3')}>

          <Text style={tw('text-base text-black mb-1')}>
            {upperFirst(item.name)}
          </Text>

          <Text style={tw('text-base text-gray-700 text-sm')}>
            {upperFirst(item.description)}
          </Text>

        </View>


      ))}
    </View>
  )
}
export default SettingsScreen
