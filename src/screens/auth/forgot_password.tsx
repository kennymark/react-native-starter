import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import tw from 'tailwind-rn'
import { SafeAreaView } from 'react-native-safe-area-context';
import { validateAll } from 'indicative/validator'
import { upperFirst } from 'lodash'
import FormErrorMessage from '../../components/Error'


export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState('')
  const [errors, setErrors] = useState(null)

  const sendResetLink = async () => {
    const data = { email }
    const rules = {
      email: 'required|email',
    }

    const messages = {
      required: (field) => `${upperFirst(field)} is required`,
      'email.email': 'Please enter a valid email address',
    }

    validateAll(data, rules, messages).then(data => {
      navigation.navigate('Login')
    }).catch(error => {
      setErrors(error)
    })

    // navigation.navigate('Signup')
  }


  return (
    <SafeAreaView style={[tw('flex-1 mt-24 items-center')]}>

      <View style={tw('w-4/5')}>
        <Text style={tw('text-2xl mb-10 text-center my-10 font-bold')}>
          Enter your email
        </Text>


        <TextInput style={tw('px-2 py-4 border border-gray-400 rounded-lg')} placeholder='example@example.com' onChangeText={(email) => setEmail(email)} />
        <FormErrorMessage errors={errors} field='email' />


        <TouchableOpacity style={tw('bg-black rounded text-gray-700 py-4 mt-4')}
          onPress={sendResetLink}>
          <Text style={tw('text-gray-300 text-center text-base font-semibold')}>Submit</Text>
        </TouchableOpacity>

      </View>


    </SafeAreaView>
  );
}
