import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import tw from 'tailwind-rn'
import { SafeAreaView } from 'react-native-safe-area-context';
import { validateAll } from 'indicative/validator'
import { upperFirst } from 'lodash'
import authService from '../../shared/services/auth.service';
import AsyncStorage from '@react-native-community/async-storage';
import FormErrorMessage from '../../components/Error'

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [errors, setErrors] = useState(null)

  const login = async () => {
    const data = { email, password }
    const rules = {
      email: 'required|email',
      password: 'required|min:4',
    }

    const messages = {
      required: (field) => `${upperFirst(field)} is required`,
      'email.email': 'Please enter a valid email address',
      'password.min': 'Password is too short',
    }

    validateAll(data, rules, messages).then(userInfo => {
      authService.login(userInfo).then(async ({ data }) => {
        const res = JSON.parse(data)
        console.log(res.token)
        await AsyncStorage.setItem('token', res.token)
      })
    }).catch(error => {
      console.log({ error })

      setErrors(error)
    })

  }




  return (
    <SafeAreaView style={[tw('flex-1 mt-24 items-center'), { fontFamily: 'Avenir' }]}>

      <View style={tw('w-4/5')}>
        <Text style={tw('text-2xl mb-10 text-center my-10 font-bold')}>
          Login
        </Text>


        <TextInput style={tw('px-2 py-4 border border-gray-400 rounded-lg')} placeholder='example@example.com' onChangeText={(email) => setEmail(email)} />

        <FormErrorMessage errors={errors} field='email' />


        <TextInput secureTextEntry style={tw('px-2 py-4 border mt-4 border-gray-400 rounded-lg')} placeholder='Password' onChangeText={(password) => setPassword(password)} />

        <FormErrorMessage errors={errors} field='password' />

        <TouchableOpacity onPress={() => navigation.push('ForgotPassword')}>
          <Text style={tw('mt-2 text-blue-600 mb-10 self-start')}>Forgot Password?
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={tw('bg-black rounded text-gray-700 py-4')}
          onPress={login}>
          <Text style={tw('text-gray-300 text-center text-base font-semibold')}>Login</Text>
        </TouchableOpacity>


        <TouchableOpacity style={tw('rounded mt-48')}
          onPress={() => navigation.navigate('Signup')}>
          <Text style={tw('text-center text-base text-gray-600')}> Don't have an account? {''}
            <Text style={tw('text-blue-500 pl-2 font-bold')}>Signup</Text>
          </Text>
        </TouchableOpacity>

        {/* <Text>{JSON.stringify({ email, password })}</Text> */}

      </View>


    </SafeAreaView>
  );
}
