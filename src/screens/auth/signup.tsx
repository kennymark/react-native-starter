import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, ScrollView, View } from 'react-native';
import tw from 'tailwind-rn'
import { SafeAreaView } from 'react-native-safe-area-context';
import { validateAll } from 'indicative/validator'
import { upperFirst } from 'lodash'
import FormErrorMessage from '../../components/Error'



export default function LoginScreen({ navigation }) {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [password_confirmation, setPasswordConfirm] = useState('')


  const [errors, setErrors] = useState(null)

  const login = async () => {
    const data = { name, email, password, password_confirmation }

    const rules = {
      name: 'required|min:5',
      email: 'required|email',
      password: 'required|min:4',
      password_confirmation: 'required|same:password'
    }

    const messages = {
      required: (field) => `${upperFirst(field)} is required`,
      'name.min': 'Name must be at least 5 characters',
      'email.email': 'Please enter a valid email address',
      'password.min': 'Password is too short',
      'password_confirmation.same': 'Passwords are not the same',

    }

    validateAll(data, rules, messages).then(data => {
      console.log({ data })
    }).catch(error => {
      console.log({ error })
      setErrors(error)
    })

    // navigation.navigate('Signup')
  }


  return (
    <SafeAreaView style={[tw('flex-1 mt-24 items-center'), { fontFamily: 'Avenir' }]}>

      <ScrollView style={tw('w-4/5')}>

        <View style={tw('my-10')}>

          <Text style={tw('text-2xl text-center font-bold')}>
            Create Account
          </Text>

          <Text style={tw('text-gray-600 my-1 text-center text-base')}>Sign up to get started</Text>
        </View>


        <TextInput style={tw('px-2 py-4 border border-gray-400 rounded-lg')} placeholder='Jane Doe' onChangeText={(email) => setName(email)} />

        <FormErrorMessage errors={errors} field='name' />


        <TextInput autoCompleteType="email" textContentType="emailAddress" keyboardType="email-address" style={tw('mt-4 px-2 py-4 border border-gray-400 rounded-lg')} placeholder='example@example.com' onChangeText={(email) => setEmail(email)} />

        <FormErrorMessage errors={errors} field='email' />



        <TextInput secureTextEntry
          style={tw('px-2 py-4 border mt-4 border-gray-400 rounded-lg')} placeholder='Password' onChangeText={(password) => setPassword(password)} />

        <FormErrorMessage errors={errors} field='password' />


        <TextInput secureTextEntry style={tw('px-2 py-4 border mt-4 border-gray-400 rounded-lg')} placeholder='Confirm Password' onChangeText={(password) => setPasswordConfirm(password)} />

        <FormErrorMessage errors={errors} field='password_confirmation' />


        <TouchableOpacity style={tw('bg-black rounded text-gray-700 py-4 mt-4')}
          onPress={login}>
          <Text style={tw('text-gray-300 text-center text-base font-semibold')}>Register</Text>
        </TouchableOpacity>


        {/* <TouchableOpacity style={tw('rounded text-gray-700 py-4')}
          onPress={() => navigation.navigate('Login')}>
          <Text style={tw('text-center text-base')}> Go to
            <Text style={tw('text-blue-500 pl-2')}> {''}Login</Text>
          </Text>
        </TouchableOpacity> */}


      </ScrollView>


    </SafeAreaView>
  );
}
